package main

import (
	"fmt"
	"io"
	"math"
	"os"
	"os/exec"
	"strconv"
	"strings"
	"sync"
)

type volman struct {
	names               []string
	procesBinNames      []string
	mediaNames          []string
	sinks               []string
	volumes             []string
	outs                []string
	outputs             []string
	fragments           []fragments
	shell               string
	pipe                string
	applicationLauncher string
	showDebug           bool
	showOutput          bool
	showHelp            bool
	printOnly           bool
	err                 []error
}

// Relic from previous version. Also faster than a map, so why not.
type fragments struct {
	fragments []string
}

func (v *volman) handleMeError(e error) {
	if !v.showDebug || e == nil {
		return
	}
	v.err = append(v.err, e)
}

func (v *volman) getInfo() *volman {
	cmd, err := exec.Command(v.shell, "-c", "pactl list sink-inputs").CombinedOutput()
	v.handleMeError(err)

	threads := strings.Split(string(cmd), "Sink Input #")
	threads = threads[1:len(threads)] //index 0 is always empty.
	tlen := len(threads)
	v.names = make([]string, tlen)
	v.mediaNames = make([]string, tlen)
	v.procesBinNames = make([]string, tlen)
	v.sinks = make([]string, tlen)
	v.volumes = make([]string, tlen)
	var wg sync.WaitGroup

	for i := range threads {
		wg.Add(1)
		go func(s string, idx int) {
			lines := strings.Split(s, "\n")

			for i := range lines {
				line := strings.ToLower(lines[i])
				if i == 0 {
					v.sinks[idx] = lines[i]
				}
				if strings.Contains(line, "application.name =") {
					v.names[idx] = strings.Split(lines[i], " = ")[1]
				}
				if strings.Contains(line, "node.name =") {
					v.names[idx] = strings.Split(lines[i], " = ")[1]
				}
				if strings.Contains(line, "application.process.binary =") {
					v.procesBinNames[idx] = strings.Split(lines[i], " = ")[1]
				}
				if strings.Contains(line, "media.name =") {
					v.mediaNames[idx] = strings.Split(lines[i], " = ")[1]
				}
				if strings.Contains(line, "volume:") {
					ret, j := 0.0, 0.0
					for _, l := range strings.Fields(lines[i]) {
						r, err := strconv.ParseFloat(l, -1)
						if err != nil || math.IsInf(r, 0) || math.IsNaN(r) {
							continue
						}
						ret += r
						j++
					}
					v.volumes[idx] = strconv.FormatFloat((ret/j)/655.36, 'f', 0, 64)
				}
			}
			wg.Done()
		}(threads[i], i)
	}
	wg.Wait()
	return v
}

func (v *volman) formNames() {
	var out strings.Builder
	sinklen := len(v.sinks) - 1
	for i := sinklen; i >= 0; i-- {
		var sb strings.Builder
		sb.WriteString(strings.ReplaceAll(v.mediaNames[i], "\"", ""))
		sb.WriteString(" (")
		sb.WriteString(strings.ReplaceAll(v.names[i], "\"", ""))
		if v.procesBinNames[i] != "" && v.procesBinNames[i] != v.names[i] {
			sb.WriteString(", ")
			sb.WriteString(strings.ReplaceAll(v.procesBinNames[i], "\"", ""))
		}
		sb.WriteString(")")
		v.names[i] = sb.String()
		out.WriteString("Sink ")
		out.WriteString(v.sinks[i])
		out.WriteString(": ")
		out.WriteString(v.names[i])
		out.WriteString(" ")
		out.WriteString(v.volumes[i])
		out.WriteString("%")
		if i > 0 {
			out.WriteString("\n")
		}
	}
	v.pipe = out.String()
	if v.printOnly {
		fmt.Println(v.pipe)
	}
}

func (v *volman) execute() {
	cmd := exec.Command(v.shell, "-c", v.applicationLauncher)
	stdin, err := cmd.StdinPipe()
	v.handleMeError(err)

	go func() {
		defer stdin.Close()
		io.WriteString(stdin, v.pipe)
	}()

	out, err := cmd.CombinedOutput()
	v.handleMeError(err)

	output := strings.Split(string(out), "\n")
	l := len(output) - 1
	v.fragments = make([]fragments, l)
	v.outs = make([]string, l)
	outs := 0

	for _, name := range v.names {
		if strings.Contains(output[outs], name) {
			v.fragments[outs].fragments = append(v.fragments[outs].fragments, strings.Split(strings.Replace(output[outs], name, "", 1), " ")...)
			var sb strings.Builder
			for i := 0; i < len(v.fragments[outs].fragments); i++ {
				if v.fragments[outs].fragments[i] == "" {
					continue
				}
				if v.fragments[outs].fragments[i] == "Sink" {
					if strings.Contains(strings.ToLower(v.fragments[outs].fragments[3]), "mute") {
						v.fragments[outs].fragments[i] = "pactl set-sink-input-mute"
					} else {
						v.fragments[outs].fragments[i] = "pactl set-sink-input-volume"
					}
				} else if strings.HasSuffix(v.fragments[outs].fragments[i], ":") {
					v.fragments[outs].fragments[i] = strings.ReplaceAll(v.fragments[outs].fragments[i], ":", "")
				} else if s := strings.ToLower(v.fragments[outs].fragments[i]); strings.Contains(s, "mute") {
					v.fragments[outs].fragments[i] = strings.ReplaceAll(s, "mute", "toggle")
				}
				sb.WriteString(v.fragments[outs].fragments[i])
				sb.WriteString(" ")
			}
			v.outs[outs] = sb.String()
			outs++
		}
	}

	v.outputs = make([]string, outs)
	for i := range v.outs {
		if v.outs[i] == "" {
			continue
		}
		cmd := exec.Command(v.shell, "-c", v.outs[i])
		var err error
		if v.showOutput {
			var o []byte
			o, err = cmd.CombinedOutput()
			v.outputs[i] = string(o)
		} else {
			err = cmd.Start()
		}
		v.handleMeError(err)
	}
}

func main() {
	var v volman
	v.shell = os.Getenv("SHELL")
	v.getFlags()
	if v.showHelp {
		var sb strings.Builder
		sb.WriteString("Use options:\n\n")
		sb.WriteString("--launcher/-l\t Allows to specify a given launcher, for example dmenu: \"volumeBrowser -l dmenu\".\n")
		sb.WriteString("--output/-o\t Prints the output of executed program, as if you used the command in shell (requires to be used in terminal).\n")
		sb.WriteString("--debug/-d\t Prints all errors gathered by the program (requires to be used in terminal).\n")
		sb.WriteString("--print/-p\t Outputs all sinks with names for scripting purposes.\n")
		fmt.Println(sb.String())
		sb.Reset()
		return
	}
	if v.applicationLauncher == "" {
		v.printOnly = true
	}

	v.getInfo().formNames()
	if !v.printOnly {
		v.execute()
	}

	if v.showOutput {
		for _, s := range v.outs {
			fmt.Println(s)
		}
	}

	if v.showDebug {
		for _, err := range v.err {
			if err != nil {
				fmt.Println(err.Error())
			}
		}
	}
}

func (v *volman) getFlags() {
	for i := 1; i < len(os.Args); i++ {
		arg := strings.ToLower(os.Args[i])
		if arg == "--launcher" || arg == "-l" {
			if len(os.Args)-1 == i {
				return
			}
			v.applicationLauncher = os.Args[i+1]
			continue
		} else if arg == "--debug" || arg == "-d" {
			v.showDebug = true
			continue
		} else if arg == "--output" || arg == "-o" {
			v.showOutput = true
			continue
		} else if arg == "--print" || arg == "-p" {
			v.printOnly = true
			continue
		} else if arg == "--help" || arg == "-h" {
			v.showHelp = true
			continue
		}
	}
}
